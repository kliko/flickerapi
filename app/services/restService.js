'use strict';

flickrApi.factory('service', function ($http, $q, baseUrl) {
    var service = {};
    var serviceUrl = baseUrl;

    function makeRequest(method, url, headers, data) {
        var defer = $q.defer();
        $http({
            method: method,
            url: url,
            headers: headers,
            data: data
        }).success(function (data, status, headers, config) {
            defer.resolve(data);
        }).error(function (data, status, headers, config) {
            defer.reject(data);
        });

        return defer.promise;
    }

    service.getUserProfile = function (url) {
        return makeRequest('GET', serviceUrl + '/Profile', this.getHeaders());
    };

    return service;
});
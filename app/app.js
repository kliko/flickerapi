'use strict';

// Declare app level module which depends on views, and components
var flickrApi = angular.module('flickrApi', [
    'ngRoute',
    'myApp.version'
]).constant('baseUrl', 'https://api.flickr.com/services/feeds/photos_public.gne?format=json')
    .config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.otherwise({redirectTo: '/view1'});

        // use the HTML5 History API
        $locationProvider.html5Mode(false)
            .hashPrefix('!');
    }]);
